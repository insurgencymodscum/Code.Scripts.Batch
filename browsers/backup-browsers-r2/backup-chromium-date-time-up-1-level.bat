@echo off

set target="C"

rem Set the current directory to CURRENT DIRECTORY.
pushd %cd%

rem ###########################################################################
rem Start a batch file without creating a new window. Using "call" b/c "start /b may create an extra window.
call backup-given-dir-date-time-up-1-level.bat %target%
rem ###########################################################################
