@echo off

set source=%1
set suffix=bak

rem Show drive, path, name, extension of current file.
echo ==========================================================================
echo Running: %~dpnx0
echo ========================================================================== & echo.

REM get current time, locale-independent, needs powershell
for /f %%a in ('powershell -Command "Get-Date -format yyMMddHHmm"') do set datetime=%%a

rem ###########################################################################
rem Get name of current directory
for %%* in (.) do set curdirname=%%~nx*
rem ###########################################################################

set target=%CD%\..\%curdirname%%suffix%\%source%%datetime%

mkdir %target%

xcopy /e /h %source% %target%

timeout 3