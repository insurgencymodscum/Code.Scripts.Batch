@echo off

set source=F24

rem Show drive, path, name, extension of current file.
echo ==========================================================================
echo Running: %~dpnx0
echo ========================================================================== & echo.

REM get current time, locale-independent, needs powershell
for /f %%a in ('powershell -Command "Get-Date -format yyMMddHHmm"') do set datetime=%%a

set target=%source%%datetime%
mkdir %target%

xcopy /e /h %source% %target%

timeout 3