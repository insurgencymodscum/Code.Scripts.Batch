@echo off

cls
echo ###########################################################################
echo Compress all VDI VirtualBox disks.
echo This is recursive.
echo http://www.trapv.com/articles/defrag_and_compact_vdi_images/
echo ###########################################################################
echo.

pause

SET VBOXMANAGE="C:\Program Files\Oracle\VirtualBox\VBoxManage.exe"

rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"

FOR /r %%i IN (*.vdi) DO (%VBOXMANAGE% modifyvdi "%%i" --compact)