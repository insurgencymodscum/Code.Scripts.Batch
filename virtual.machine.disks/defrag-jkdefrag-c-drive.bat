rem JKDefrag:
rem 7 = Optimize by sorting all files by name (folder + filename).
rem 3 = Defragment and fast optimize [recommended].

JkDefragCmd64.exe -a 7 c: &
JkDefragCmd64.exe -a 3 c: &
pause