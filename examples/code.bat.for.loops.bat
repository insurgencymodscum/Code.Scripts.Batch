@echo off

rem ##########################################################################

rem Recursive!
rem Note: use "start /b" inside the loop!

for /r "%CD%" %%a in (*.*) do (

rem Delete matching files.
echo Deleting: %%~a & del "%%~a"

rem Perform command on matching files.
start /b do.exe %%~a
)

rem ###########################################################################

rem Current dir.
for %%a in (*.*) do (
echo "%%~fnxa"
)

rem ###########################################################################

rem Delete matching dir.
FOR /d /r %%b IN (Logs) DO @if exist "%%b" echo Deleting: "%%b" & rd "%%b"

rem ###########################################################################

rem Start/kill specified programs.

set kill=false
set run=true

for %%x in (
	Folder\Program.exe
	Folder\Program.exe
) do (
	if %run% == true (echo %%x & echo. & start /b /min %%x & timeout 1 & echo.)
	if %kill% == true (echo %%x & echo. & taskkill /f /im %%x & timeout 1 & echo.)
	)

rem ###########################################################################