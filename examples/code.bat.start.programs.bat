@echo off

rem ###########################################################################

rem Set the current directory to CURRENT DIRECTORY.
pushd %cd%

rem Change directory back to the path/folder most recently stored by the PUSHD command.
popd

rem ###########################################################################

rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"

rem Start an application without creating a new window.
start /b

rem Start an application and wait for it to terminate.
start /wait

rem Start an application without creating a new window & wait for it to terminate.
start /b /wait

rem Start an application IN CURRENT DIRECTORY without creating a new window.
start /b  %~dp0\

rem Start an application IN CURRENT DIRECTORY and wait for it to terminate.
start /wait %~dp0\

rem Start an application IN CURRENT DIRECTORY without creating a new window & wait for it to terminate.
start /b /wait %~dp0\

rem Start a batch file without creating a new window. Using "call" b/c "start /b may create an extra window.
call

rem ###########################################################################