@echo off

rem ###########################################################################

rem /f Force deleting of read-only files.
rem /q Quiet mode, do not ask if ok to delete a global wildcard.
del /f /q

rem ###########################################################################

rem /s Recursive. Removes all directories and files in the specified directory in addition to the directory itself. Used to remove a directory tree.
rem /q Quiet mode, do not ask if ok to remove a direcotry tree with /s
rd /s /q

rem ###########################################################################

rem If folder exists, delete it.
if exist "foldername" echo Deleting... & rmdir /s /q "foldername"

rem ###########################################################################