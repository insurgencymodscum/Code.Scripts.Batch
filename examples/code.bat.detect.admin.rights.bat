rem ###########################################################################

rem Detect admin rights.

ECHO Administrative permissions required. Detecting permissions...
net session >nul 2>&1
if %errorLevel% == 0 (
ECHO Success: Administrative permissions confirmed.
pause
cls
) else (
ECHO Failure: Current permissions inadequate. Script Terminated.
Pause
Exit
)

rem ###########################################################################