echo ========================================================================== & echo.

echo Drive, path, name, extension of current file.
echo %~dpnx0

echo ========================================================================== & echo.

echo Drive, path, name, extension of first argument.
echo %~dpnx1

echo ========================================================================== & echo.

echo All arguments.
echo %*

rem =============================================

echo The %~nx0 args are...
for %%I IN (%*) DO ECHO %%I

rem =============================================

rem Check argument value.

set arg1=%~1

if "%arg1%" == "foo" (echo bar)

rem =============================================