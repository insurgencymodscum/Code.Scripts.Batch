rem ##########################################################################
rem Assign command output to a variable. Sometimes does not work!
for /f "delims=" %%i in ('command') do set output=%%i
echo %output%
rem ##########################################################################

rem ##########################################################################
rem Assign command output to a variable. Usually works!
command > tmp.txt
set /p out=<tmp.txt
echo %out%
rem ##########################################################################
