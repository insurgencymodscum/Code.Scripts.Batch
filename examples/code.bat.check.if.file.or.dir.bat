rem ###########################################################################

rem USUALLY WORKS!

set targets=*.md,*.html,

rem Check if input (%1) is a file or directory.
dir /ad /b %1 1> NUL 2> NUL
if %ERRORLEVEL% EQU 0 (
	echo Input is a directory.
	for /r  "%1" %%a in (%targets%) do (
		echo ==========================================================================
		echo File "%%~nxa"
		call :dostuff %%a
		echo ==========================================================================
		)
) else (
	echo Input is a file.
	echo ==========================================================================
	echo File: "%~nx1"
	call :dostuff %~dpnx1
	echo ==========================================================================
)

exit /b

rem ###########################################################################

REM SOMETIMES DOES NOT WORK!

rem Check if input is a file or directory
forfiles /s /P "%1" /C "cmd /c if @isdir==TRUE (echo @path is a folder) else (echo @file is a file)"

rem ###########################################################################
