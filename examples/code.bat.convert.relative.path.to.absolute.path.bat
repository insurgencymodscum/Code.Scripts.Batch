rem ###########################################################################

rem Convert relative path to absolute path.

set relativepath=%CD%\..\..\..\..\..\..
set absolutepath=

echo %relativepath%

call :relativetoabsolutepath "%relativepath%"

echo %absolutepath%

rem ===========================================================================
rem Convert relative path to absolute path.
:relativetoabsolutepath
set "absolutepath=%~f1"
GOTO :EOF
rem ===========================================================================

rem ###########################################################################