rem ##########################################################################

rem Append command output to a given file.

for /f "delims=" %%i in ('program.exe %1') do set output=%%i

echo %output% >> %1

rem ##########################################################################