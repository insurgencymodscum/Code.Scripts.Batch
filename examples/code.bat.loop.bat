@echo off

rem ###########################################################################

rem 1 hour
set /a looptime= 1 * 60 * 60 * 1000

:beginloop

ping 192.0.2.1 -n 1 -w %looptime% > nul

echo Starting...

goto beginloop
rem ###########################################################################