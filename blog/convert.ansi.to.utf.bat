@echo off

echo ###########################################################################
echo Convert specified file or folder from Unicode (UTF-8) to ANSI (CP1252) via iconv.
echo ###########################################################################
echo.

set targets=*.md,*.html,

rem Check if input (%1) is a file or directory.
dir /ad /b %1 1> NUL 2> NUL
if %ERRORLEVEL% EQU 0 (
	echo Input is a directory.
	pause
	for /r  "%1" %%a in (%targets%) do (
		echo ==========================================================================
		echo File "%%~nxa"
		call :doit %%a
		echo ==========================================================================
		)
) else (
	echo Input is a file.
	pause
	echo ==========================================================================
	echo File: "%~nx1"
	call :doit %~dpnx1
	echo ==========================================================================
)

exit /b

rem ###########################################################################

:doit

echo Args:%* & echo.

pushd %~dp1

rem convert from utf-8 (unicode) to CP1252 (ansi)
rem -c = discard unconvertable chars
iconv -f cp1252 -t utf-8 -c "%1" > "%1.ansi"