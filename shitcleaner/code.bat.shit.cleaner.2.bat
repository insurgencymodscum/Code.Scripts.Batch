@echo off

rem ###########################################################################

rem Drive, path, name, extension of current file.
TITLE %~dpnx0

set target=%CD%\..\
set targetabsolute=
set shit=*.old,*.tmp,*.bak,*.log,*.dmp

rem echo cleaning %target%

rem convert relative to absolute path
call :relativetoabsolutepath "%target%"

echo ==========================================================================
echo Cleaning %targetabsolute%
echo ==========================================================================
echo.

for /r "%targetabsolute%" %%a in (%shit%) DO (
echo %%a
del "%%a"
)

rem ===========================================================================
rem Convert relative path to absolute path.
:relativetoabsolutepath
set "targetabsolute=%~f1"
GOTO :EOF
rem ===========================================================================

rem ###########################################################################