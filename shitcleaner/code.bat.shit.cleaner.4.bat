@echo off

set comment=DANGER!

rem ===========================================================================
set target=%cd%
rem Total Commander: *.old;*.tmp;*.bak;*.log;*.dmp;
set shit=*.old,*.tmp,*.bak,*.log,*.dmp
rem ===========================================================================

rem ###########################################################################
rem This prevents conflict with timeout in Cygwin, MinGW, etc.
set timeout=%SystemRoot%\System32\timeout.exe
rem ###########################################################################

rem ###########################################################################
rem Show drive, path, name, extension of current file, comments.
title %~dpnx0
cls
echo ==========================================================================
echo Running %~dpnx0
echo %comment%
echo ========================================================================== & echo.
pause
pause
pause
rem ###########################################################################

rem ===========================================================================
rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"
rem ===========================================================================

rem ===========================================================================
rem convert relative to absolute path
call :relativetoabsolutepath "%target%"
rem ===========================================================================

echo ==========================================================================
echo Cleaning %targetabsolute%
echo ==========================================================================
echo.

pause

rem Delete matching files.
for /r "%targetabsolute%" %%a in (%shit%) DO (
echo Deleting: %%a
rem Must be in quotes!
del "%%a"
)

timeout 3

rem ===========================================================================
rem Convert relative path to absolute path.
:relativetoabsolutepath
set "targetabsolute=%~f1"
GOTO :EOF
rem ===========================================================================