@echo off

rem ===========================================================================
rem Drive, path, name, extension of current file.
TITLE %~dpnx0
rem ===========================================================================

rem ===========================================================================
set target=%CD%
set targetabsolute=
set shitfiles==*.old,*.tmp,*.bak,*.log,*.dmp

rem ===========================================================================
rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"
rem ===========================================================================

rem ===========================================================================
rem convert relative to absolute path
call :relativetoabsolutepath "%target%"
rem ===========================================================================

echo ==========================================================================
echo Cleaning %targetabsolute%
echo ==========================================================================
echo.

pause

rem Delete matching files.
for /r "%targetabsolute%" %%a in (%shitfiles%) DO echo Deleting: %%a & del "%%a"

rem ===========================================================================
rem Convert relative path to absolute path.
:relativetoabsolutepath
set "targetabsolute=%~f1"
GOTO :EOF
rem ===========================================================================